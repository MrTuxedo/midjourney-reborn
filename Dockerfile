FROM node:slim

WORKDIR /app

COPY package*.json ./
RUN npm install --omit=dev

COPY . .

CMD node index.js